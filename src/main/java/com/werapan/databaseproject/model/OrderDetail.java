/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import com.werapan.databaseproject.dao.ProductDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class OrderDetail {

    public static OrderDetail fromRS(ResultSet rs) {
        ProductDao productDao = new ProductDao();

        OrderDetail orderdetail = new OrderDetail();
        try {
            orderdetail.setId(rs.getInt("order_detail_id")); // PK OrderDetail

            int productId = rs.getInt("product_id"); // FK Product
            Product item = productDao.get(productId); // FK Product

            orderdetail.setQty(rs.getInt("order_detail_qty"));
            orderdetail.setProduPrice(rs.getDouble("product_price"));
            orderdetail.setProductname(rs.getString("product_name"));

            //   orderdetail.setProduct(product);
            //  orderdetail.setTotal(rs.getDouble("product_name"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);

        }

        return orderdetail;
    }
    private int id; // PK
    Product product;
    private String productname;
    private double produPrice;
    private int qty;
    private Orders orderdetail; //FK

    public OrderDetail(int id, Product product, String productname, double produPrice, int qty, Orders orderdetail) {
        this.id = id;
        this.product = product;
        this.productname = productname;
        this.produPrice = produPrice;
        this.qty = qty;
        this.orderdetail = orderdetail;
    }

    public OrderDetail(Product product, String productname, double produPrice, int qty, Orders orderdetail) {
        this.product = product;
        this.productname = productname;
        this.produPrice = produPrice;
        this.qty = qty;
        this.orderdetail = orderdetail;
    }

    public OrderDetail() {
        this.id = -1;
    }

    @Override
    public String toString() {
        return "OrderDetail" + "id=" + id + ", product=" + product + ", productname=" + productname + ", produPrice=" + produPrice + ", qty=" + qty + ", orderdetail=" + orderdetail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public double getProduPrice() {
        return produPrice;
    }

    public void setProduPrice(double produPrice) {
        this.produPrice = produPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Orders getOrder() {
        return orderdetail;
    }

    public void setOrder(Orders orderdetail) {
        this.orderdetail = orderdetail;
    }

    public double getTotal() {
        return qty * produPrice;
    }

}
