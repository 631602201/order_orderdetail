--
-- File generated with SQLiteStudio v3.3.3 on ส. ต.ค. 1 15:05:47 2022
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: category
DROP TABLE IF EXISTS category;

CREATE TABLE category (
    category_id   INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    category_name TEXT (50) NOT NULL
);

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         1,
                         'MyCoffee'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         2,
                         'dessert'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         3,
                         'candy'
                     );


-- Table: order_detial
DROP TABLE IF EXISTS order_detial;

CREATE TABLE order_detial (
    order_detial_id  INTEGER   PRIMARY KEY,
    product_id       INTEGER   REFERENCES product (product_id) ON DELETE CASCADE
                                                               ON UPDATE CASCADE,
    order_detial_qty INT,
    product_price    DOUBLE,
    product_name     TEXT (50),
    order_id         INTEGER   REFERENCES orders (order_id) ON DELETE CASCADE
                                                            ON UPDATE CASCADE
);

INSERT INTO order_detial (
                             order_detial_id,
                             product_id,
                             order_detial_qty,
                             product_price,
                             product_name,
                             order_id
                         )
                         VALUES (
                             1,
                             1,
                             1,
                             75.0,
                             'A',
                             1
                         );

INSERT INTO order_detial (
                             order_detial_id,
                             product_id,
                             order_detial_qty,
                             product_price,
                             product_name,
                             order_id
                         )
                         VALUES (
                             2,
                             3,
                             1,
                             70.0,
                             'C',
                             1
                         );

INSERT INTO order_detial (
                             order_detial_id,
                             product_id,
                             order_detial_qty,
                             product_price,
                             product_name,
                             order_id
                         )
                         VALUES (
                             3,
                             2,
                             1,
                             180.0,
                             'B',
                             1
                         );


-- Table: orders
DROP TABLE IF EXISTS orders;

CREATE TABLE orders (
    order_id    INTEGER  PRIMARY KEY ASC AUTOINCREMENT,
    order_date  DATETIME DEFAULT (DATETIME('now', 'localtime') ),
    order_total DOUBLE,
    order_qty   INT
);

INSERT INTO orders (
                       order_id,
                       order_date,
                       order_total,
                       order_qty
                   )
                   VALUES (
                       1,
                       '2022-10-01 01:56:15',
                       315.0,
                       3
                   );


-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE product (
    product_id    INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    product_name  TEXT (50) UNIQUE,
    product_price DOUBLE    NOT NULL
);

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price
                    )
                    VALUES (
                        1,
                        'A',
                        75.0
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price
                    )
                    VALUES (
                        2,
                        'B',
                        180.0
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price
                    )
                    VALUES (
                        3,
                        'C',
                        70.0
                    );


-- Table: user
DROP TABLE IF EXISTS user;

CREATE TABLE user (
    user_id       INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    user_login    TEXT (50) UNIQUE,
    user_gender   TEXT (3)  NOT NULL,
    user_password TEXT (50) NOT NULL,
    user_role     INTEGER   NOT NULL,
    user_name     TEXT (50) 
);

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name
                 )
                 VALUES (
                     6,
                     'werapan',
                     'M',
                     'password',
                     1,
                     'Worawit'
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name
                 )
                 VALUES (
                     10,
                     'admin',
                     'M',
                     'password',
                     0,
                     'Administrator'
                 );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
